# DBApi插件开发案例

[DBApi](https://gitee.com/freakchicken/db-api) 插件开发案例

# 概述
- 随着DBApi的版本更新，插件也会随版本更新，请查看对应版本的插件demo

# 使用方式
```xml
<dependency>
    <groupId>com.gitee.freakchicken.dbapi</groupId>
    <artifactId>dbapi-plugin</artifactId>
    <version>3.0.0</version>
    <scope>provided</scope>
</dependency>
```